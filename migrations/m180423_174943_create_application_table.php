<?php

use yii\db\Migration;

/**
 * Handles the creation of table `application`.
 */
class m180423_174943_create_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%application}}', [
            'id' => $this->primaryKey(),
            'title'=>$this->string(250),
            'content'=>$this->text(),
            'lang'=>$this->string(10),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%application}}');
    }
}

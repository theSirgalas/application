<?php

use yii\db\Migration;
use app\models\Application;
/**
 * Handles the creation of table `add_column_status_from_application`.
 */
class m180423_190016_create_add_column_status_from_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Application::tableName(),'status',$this->tinyInteger(3));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(Application::tableName(),'status');
    }
}

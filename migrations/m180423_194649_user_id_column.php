<?php

use yii\db\Migration;
use app\models\Application;
use app\models\User;

/**
 * Class m180423_194649_user_id_column
 */
class m180423_194649_user_id_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // drops foreign key for table `application`
        $this->dropForeignKey(
            'fk-application_user-application_id',
            'application_user'
        );

        // drops index for column `application_id`
        $this->dropIndex(
            'idx-application_user-application_id',
            'application_user'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-application_user-user_id',
            'application_user'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-application_user-user_id',
            'application_user'
        );

        $this->dropTable('application_user');
        
        $this->addColumn(Application::tableName(),'user_id',$this->integer());
        
        $this->addForeignKey(
            'fk-application_user-user_id',
            'application',
            'user_id',
            User::tableName(),
            'id',
            'CASCADE'
        );
    
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('{{%application_user}}', [
            'application_id' => $this->integer(),
            'user_id' => $this->integer(),
            'PRIMARY KEY(application_id, user_id)',
        ]);

        // creates index for column `application_id`
        $this->createIndex(
            'idx-application_user-application_id',
            'application_user',
            'application_id'
        );

        // add foreign key for table `application`
        $this->addForeignKey(
            'fk-application_user-application_id',
            'application_user',
            'application_id',
            Application::tableName(),
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-application_user-user_id',
            'application_user',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-application_user-user_id',
            'application_user',
            'user_id',
            User::tableName(),
            'id',
            'CASCADE'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180423_194649_user_id_column cannot be reverted.\n";

        return false;
    }
    */
}

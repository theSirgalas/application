<?php

use yii\db\Migration;
use app\models\Application;
use app\models\User;
/**
 * Handles the creation of table `application_user`.
 * Has foreign keys to the tables:
 *
 * - `application`
 * - `user`
 */
class m180423_183604_create_junction_table_for_application_and_user_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%application_user}}', [
            'application_id' => $this->integer(),
            'user_id' => $this->integer(),
            'PRIMARY KEY(application_id, user_id)',
        ]);

        // creates index for column `application_id`
        $this->createIndex(
            'idx-application_user-application_id',
            'application_user',
            'application_id'
        );

        // add foreign key for table `application`
        $this->addForeignKey(
            'fk-application_user-application_id',
            'application_user',
            'application_id',
            Application::tableName(),
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-application_user-user_id',
            'application_user',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-application_user-user_id',
            'application_user',
            'user_id',
            User::tableName(),
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `application`
        $this->dropForeignKey(
            'fk-application_user-application_id',
            'application_user'
        );

        // drops index for column `application_id`
        $this->dropIndex(
            'idx-application_user-application_id',
            'application_user'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-application_user-user_id',
            'application_user'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-application_user-user_id',
            'application_user'
        );

        $this->dropTable('application_user');
    }
}

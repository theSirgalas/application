<?php

use yii\db\Migration;
use app\models\Application;
/**
 * Handles the creation of table `answer`.
 */
class m180424_042128_create_answer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%answer}}', [
            'id' => $this->primaryKey(),
            'title'=>$this->string(255)->notNull(),
            'content'=>$this->text()->notNull(),
            'user_title'=>$this->string(255),
            'content_user'=>$this->text(),
            'application_id'=>$this->integer()->notNull()
        ]);

        $this->addForeignKey(
            'fk-answer-application-id',
            'answer',
            'application_id',
            Application::tableName(),
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('answer');
    }
}

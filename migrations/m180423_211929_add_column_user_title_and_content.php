<?php

use yii\db\Migration;
use app\models\Application;
/**
 * Class m180423_211929_add_column_user_title_and_content
 */
class m180423_211929_add_column_user_title_and_content extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Application::tableName(),'user_content',$this->string(255));
        $this->addColumn(Application::tableName(),'user_title',$this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(Application::tableName(),'user_content');
        $this->dropColumn(Application::tableName(),'user_title');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180423_211929_add_column_user_title_and_content cannot be reverted.\n";

        return false;
    }
    */
}

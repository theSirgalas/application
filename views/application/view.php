<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Application */
/* @var $answer app\models\Answer */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' =>'title',
                'value' => function()use($model){
                    return $model->user_title;
                }
            ],
            [
                'attribute'=>'content',
                'value'=> function()use($model){
                    return $model->user_content;
                }
            ],
            [
                'attribute'=>'status',
                'value' => function()use($model){
                    return $model->getStatus();
                }
            ],
        ],
    ]) ?>
    <h2>Answer</h2>
    <?= DetailView::widget([
        'model' => $answer,
        'attributes' => [
            [
                'attribute' =>'title',
                'value' => function()use($model){
                    return $model->user_title;
                }
            ],
            [
                'attribute'=>'content',
                'value'=> function()use($model){
                    return $model->user_content;
                }
            ]
        ],
    ]) ?>

</div>

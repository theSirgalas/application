<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\ApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Applications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Application', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' =>'title',
                'value' => function($model){
                    return $model->user_title;
                }
            ],
            [
                'attribute'=>'status',
                'format' => 'raw',
                'filter' => Html::dropDownList('App;ication[status]', $searchModel["status"], $applicationStatus, ['prompt' => '', 'class' => 'form-control', 'encodeSpaces' => true]),
                'value' => function($model){
                    return $model->getStatus();
                }
            ],


            ['class' => 'yii\grid\ActionColumn',
              'template'=>'{view}'
            ],
        ],
    ]); ?>
</div>

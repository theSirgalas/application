<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Application */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'content:ntext',
            [
                'attribute'=>'status',
                'format' => 'raw',
                'value' => function()use($model){
                    return $model->getStatus();
                }
            ],
            [
                'attribute'=>'user_id',
                'value' => function()use($model){
                    return $model->user->username;
                }
            ],
        ],
    ]) ?>

</div>

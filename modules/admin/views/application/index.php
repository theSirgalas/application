<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\search\ApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Applications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Application', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',

            [
                'attribute'=>'status',
                'format' => 'raw',
                'value' => function($model){
                    return $model->getStatus();
                }
            ],
            [
                'attribute'=>'user_id',
                'value' => function($model){
                    return $model->user->username;
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update},{view},{answer},{answer_update}',
                'buttons' => [
                    'answer' =>  function ($url, $model) {
                        return Html::a('<i class="fa fa-user-o"></i>', \yii\helpers\Url::to(['/admin/answer/create', 'id' =>$model->id]));
                    },
                    'answer_update' =>  function ($url, $model) {
                        return Html::a('<i class="fa fa-users"></i>', \yii\helpers\Url::to(['/admin/answer/update','id'=>$model->id]));
                    },
                ]
            ],
        ],
    ]); ?>
</div>
